import os


def move_profile_folder(filename, base_directory):
    folder_name = "profile_files"
    profile_full_path = base_directory + folder_name + "/"
    src = base_directory + filename

    if os.path.isfile(src) is False:
        return -1

    create_if_not_exist(profile_full_path)

    dst = profile_full_path + filename
    os.rename(src, dst)


def create_if_not_exist(full_path):
    if os.path.exists(full_path) is False:
        os.mkdir(full_path)


def order_files(original_path):
    all_files = os.listdir(original_path)

    for c_files in all_files:
        full_path = original_path + c_files

        if c_files[:2] == "20":
            file_n_spliteada = c_files.split("-")

            if len(file_n_spliteada) > 1:
                y = file_n_spliteada[0]
                m = file_n_spliteada[1]
                d = file_n_spliteada[2]

                # Folder YEAR
                folder_full_path = original_path + y + "/"
                create_if_not_exist(folder_full_path)

                # Folder MONTH
                folder_full_path = original_path + y + "/" + m + "/"
                create_if_not_exist(folder_full_path)

                dst_file = folder_full_path + c_files

                print("Moving from " + full_path + " to " + dst_file)
                os.rename(full_path, dst_file)

        else:
            move_profile_folder(c_files, original_path)




def main():
    # check_folders = ["/root/stories/", "/root/annaphoenix/", "/root/diana_yevdokimova/", "/root/elena__istomina__/", "/root/francesco.nazari.fusetti/"]
    check_folders = ["/content/drive/My Drive/stories/", "/content/drive/My Drive/annaphoenix/", "/content/drive/My Drive/diana_yevdokimova/", "/content/drive/My Drive/elena__istomina__/", "/content/drive/My Drive/francesco.nazari.fusetti/"]

    for c_folder in check_folders:
        print("Checking: " +c_folder)
        if os.path.exists(c_folder):
            order_files(c_folder)

main()